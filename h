#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# h generation 3
# SPDX-License-Identifier: GPL-3.0
# Copyright (c) 2009-2020 Jérôme Carretero & contributors

import io, sys, os, time, syslog, re, socket, marshal, logging, contextlib

histdir = os.path.join(os.environ['HOME'], '.hist')

histpath = os.environ.get("H_HISTORY_PATH", "").split(os.pathsep)
if histdir not in histpath:
	histpath += [histdir]


logger = logging.getLogger(None if __name__ == "__main__" else __name__)


@contextlib.contextmanager
def h3_connection():
	ensure_server()

	sname = socket_path() + ".socket"
	s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	s.connect(sname)
	yield s
	s.close()

def socket_path():
	tmp = os.environ.get("XDG_RUNTIME_DIR", '/run/user/%d' % os.getuid())
	if not os.path.exists(tmp):
		tmp = '/tmp/user/%d' % os.getuid()

	base = os.path.join(tmp, "h3", "%08x" % sys.hexversion)
	return base


def ensure_server():
	sbase = socket_path()
	sname = sbase + ".socket"

	do_fork = True
	if not os.path.exists(sname):
		pass
	else:
		try:
			s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
			s.connect(sname)
			s.send(marshal.dumps("hello"))
			s.close()
			do_fork = False
		except socket.error:
			pass

	if do_fork:
		pid = os.fork()
		if pid == 0:
			logging.info("Forked")
			os.chdir("/")
			os.setsid()
			os.umask(0)
			sys.argv = ['h3', 'daemon']
			pid2 = os.fork()
			if pid2 == 0:
				logging.info("Forked again")

				if sys.hexversion < 0x03000000:
					os.close(0)
					os.close(1)
					os.close(2)

				folder = os.path.dirname(sname)
				if not os.path.exists(folder):
					os.makedirs(folder)

				out_log = io.open(sbase + '.out', 'ab+')
				err_log = io.open(sbase + '.err', 'ab+', 0)
				dev_null = io.open(os.devnull, 'rb')

				os.dup2(out_log.fileno(), sys.stdout.fileno())
				os.dup2(err_log.fileno(), sys.stderr.fileno())
				os.dup2(dev_null.fileno(), sys.stdin.fileno())

				s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

				if os.path.exists(sname):
					os.remove(sname)

				s.bind(sname)
				s.listen(1)
				stop = False
				while not stop:
					conn, addr = s.accept()
					def send(x):
						try:
							conn.send(x)
						except:
							pass

					with conn.makefile(mode="rb") as fi:
						cmd, *args = marshal.load(fi)

					if cmd == "q":
						logger.info("Quitting")
						stop = True
					elif cmd == "pid":
						send(str(os.getpid()).encode())
					elif cmd == "s":
						d = " ".join(args).encode("utf-8", errors="surrogateescape")
						def printbuf(x):
							if isinstance(x, str):
								x = x.encode("utf-8", errors="surrogateescape")
							send(b's ' + x)
						printbuf(b"e %s" % d)
						history_search(d, printbuf)
						send(b'q')
					elif cmd == "sl":
						d = " ".join(args).encode("utf-8", errors="surrogateescape")
						def printbuf(x):
							if isinstance(x, str):
								x = x.encode("utf-8", errors="surrogateescape")
							send(b'sl ' + x)
						printbuf(b"e %s" % d)
						history_search(d, printbuf, last=True)
						send(b'q')
					elif cmd == "a":
						d = " ".join(args).encode("utf-8", errors="surrogateescape")
						history_append(d)
						send(b'q')
					elif cmd == "l":
						def printbuf(x):
							if isinstance(x, str):
								x = x.encode("utf-8", errors="surrogateescape")
							send(b'l ' + x)
						printbuf(b"e l")
						history_search(b".*", printbuf, verbose=False, last=True, show_date=False)
						send(b'q')
					elif cmd == "p":
						history_prune()
					else:
						send(b'?')
					conn.close()
				sys.exit()
			else:
				sys.exit()
		else:
			# Fork was launched
			s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
			trials = 5
			for i in range(trials):
				try:
					logging.info("Connecting %s..." % sname)
					s.connect(sname)
					break
				except:
					time.sleep(0.3)
					if i == trials-1:
						raise
			s.send(marshal.dumps('hello'))
			s.close()


def is_old_repeated(old, cmd, repeat):
	if len(old) < 2*repeat-1:
		return False
	a0 = 2*repeat-1
	a1 = repeat-1
	b0 = repeat-1
	a = old[-a0:-a1]
	b = old[-b0:]
	if repeat == 1:
		a = [old[-1]]
		b = []
	res = a == b + [cmd]
	if 1:
		l = "\x1B[31m%s \x1B[33m%s \x1B[35m%s \x1B[0m %d %d %d %d %d" \
		 % (" ".join(a), " ".join(b), cmd, repeat, a0, a1, b0, res)
		print(" repeated %d %s" % (repeat, l))
	return res


def history_prune():
	for cwd, dirs, files in os.walk(histdir):
		files = sorted(files)
		for filename in files:
			if filename.endswith((".xz", ".zstd")):
				continue
			if "bak" in filename:
				continue
			print("File: %s" % filename)
			cf = os.path.join(cwd, filename)
			with open(cf, 'r') as f:
				lines = f.readlines()
			with open(cf, 'w') as f:
				old = []
				buf = []
				buf_len = 10
				rep_len = 5
				for idx_line, line in enumerate(lines):
					m = re.match(br"^(?P<date>\S+)\s+(?P<line>.*)$", line)
					if m:
						cmd = m.group('line')
						print("%05d: %s" % (idx_line, cmd))
						old_p = False
						for i in range(1, rep_len):
							old_p |= is_old_repeated(old, cmd, i)

						if cmd in ('lk', 'l'):
							print(" removed")
							continue
						elif old_p:
							print(" old")
							continue
						else:
							pass

						# Flush old entries that cannot be repeated
						while len(buf) > rep_len-1:
							f.write(buf[0])
							buf = buf[1:]

						old.append(cmd)
						buf.append(line)
						old = old[-buf_len:]

					else:
						f.write(line)

				for line in buf:
					f.write(line)


def history_append(cmd):
	if "H_SYSLOG" in os.environ:
		def write(data):
			syslog.openlog(os.environ["LOGNAME"] + "@" + os.uname()[1] + "/" + os.path.basename(os.environ["SHELL"]))
			syslog.syslog(1, data)
			syslog.closelog()
	else:
		def write(data):
			shell = os.path.basename(os.environ['SHELL'])
			histfile = os.path.join(histdir, shell)
			with io.open(histfile, 'ab') as f:
				f.write(data)

	stop = time.strftime("%Y%m%d@%H%M%S").encode()
	m = re.match(br'^(?P<num>\s*\d+\s+)?(?P<start>\d+@\d+) (?P<cmd>.+)$', cmd)
	if m:
		txt = b'%s-%s %s\n' % (m.group('start'), stop, m.group('cmd'))
	else:
		txt = b'%s %s\n' % (stop, cmd)
	#print("Writing %s from %s" % (txt, cmd))
	write(txt)


def history_print(line, printbuf, show_date=True):
	m = re.match(br"^(?P<date>\S+)\s+(?P<line>.*)$", line)
	if m is None:
		return
	g = m.group
	if show_date:
		s = b"[33m%s[0m %s\n" % (g('date'), g('line'))
	else:
		s = b"%s\n" % (g('line'))
	#s = s.encode('utf-8', 'xmlcharrefreplace')
	printbuf(s)


def history_search(term, printbuf, **kw):

	last = kw.get('last', False)
	verbose = kw.get('verbose', True)
	show_date = kw.get('show_date', True)

	for hist_dir in histpath:
		import subprocess
		for cwd, dirs, files in os.walk(hist_dir):
			dirs[:] = []
			for filename in sorted(files):
				cf = os.path.join(cwd, filename)
				def check(x):
					m = re.match(br"^(?P<date>\S+)\s+(?P<line>.*)$", x)
					if m:
						cmd = m.group('line')
						if re.search(term, cmd):
							history_print(x, printbuf, show_date=show_date)

				if filename.startswith("."):
					continue

				elif cf.endswith('.xz'):
					if not last:
						if verbose:
							printbuf("File: %s\n" % cf)
						proc = subprocess.Popen(['xz', '-dc', cf], stdout=subprocess.PIPE)
						for line in proc.stdout:
							check(line)
						proc.wait()

				elif cf.endswith('.zstd'):
					if not last:
						if verbose:
							printbuf("File: %s\n" % cf)
						proc = subprocess.Popen(['zstd', '-dc', cf], stdout=subprocess.PIPE)
						for line in proc.stdout:
							check(line)
						proc.wait()

				else:
					if verbose:
						printbuf("File: %s\n" % cf)
					with open(cf, "rb") as f:
						for line in f:
							check(line)


def printb(x):
	if sys.hexversion >= 0x03000000:
		sys.stdout.buffer.write(x)
		sys.stdout.buffer.write(b"\n")
	else:
		sys.stdout.write(x)
		sys.stdout.write(b"\n")
	sys.stdout.flush()


def main():
	import argparse

	parser = argparse.ArgumentParser(
	 description="History manager",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)


	subp = subparsers.add_parser(
	 "a",
	 help="Add an entry",
	)

	subp.add_argument('entry',
	 nargs=argparse.REMAINDER,
	)


	def do_add(args):
		logger.info("Adding %s", args.entry)
		with h3_connection() as s:
			d = marshal.dumps(["a"] + args.entry)
			s.send(d)

	subp.set_defaults(func=do_add)


	subp = subparsers.add_parser(
	 "s",
	 help="Search in whole history",
	)

	def do_search_all(args):
		with h3_connection() as s:
			d = marshal.dumps(["s"] + args.entry)
			s.send(d)
			with s.makefile(mode="rb") as f:
				for line in f:
					line = line.rstrip()
					if line == b'q':
						break
					printb(line[2:])

	subp.set_defaults(func=do_search_all)

	subp.add_argument('entry',
	 nargs=argparse.REMAINDER,
	)


	subp = subparsers.add_parser(
	 "sl",
	 help="Search last history file",
	)

	def do_search_last(args):
		with h3_connection() as s:
			d = marshal.dumps(["sl"] + args.entry)
			s.send(d)
			with s.makefile(mode="rb") as f:
				for line in f:
					line = line.rstrip()
					if line == b'q':
						break
					printb(line[2:])

	subp.set_defaults(func=do_search_last)

	subp.add_argument('entry',
	 nargs=argparse.REMAINDER,
	)


	subp = subparsers.add_parser(
	 "l",
	 help="Load history",
	)

	def do_load(args):
		with h3_connection() as s:
			d = marshal.dumps(["l"] + args.entry)
			s.send(d)
			with s.makefile(mode="rb") as f:
				for line in f:
					line = line.rstrip()
					if line == b'q':
						break
					printb(line[2:])

	subp.set_defaults(func=do_load)

	subp.add_argument('entry',
	 nargs=argparse.REMAINDER,
	)


	subp = subparsers.add_parser(
	 "p",
	 help="Prune history",
	)

	def do_prune(args):
		history_prune()

	subp.set_defaults(func=do_prune)

	subp.add_argument('entry',
	 nargs=argparse.REMAINDER,
	)


	subp = subparsers.add_parser(
	 "q",
	 help="Quit",
	)

	def do_q(args):
		with h3_connection() as s:
			d = marshal.dumps(['q'])
			s.send(d)

	subp.set_defaults(func=do_q)


	subp = subparsers.add_parser(
	 "pid",
	 help="Show PID",
	)

	def do_pid(args):
		with h3_connection() as s:
			d = marshal.dumps(['pid'])
			s.send(d)
			data = s.recv(1024)
			printb(data)

	subp.set_defaults(func=do_pid)


	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.root.setLevel(getattr(logging, args.log_level))

	if 1:
		logging.basicConfig(
		 level=getattr(logging, args.log_level),
		 format="%(levelname)s %(message)s"
		)

	if getattr(args, 'func', None) is None:
		parser.print_help()
		raise SystemExit(1)
	else:
		args.func(args)
		raise SystemExit()


if __name__ == '__main__':
	ret = main()
	raise SystemExit(ret)
